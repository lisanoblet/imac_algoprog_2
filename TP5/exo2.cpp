#include <QApplication>
#include <QString>
#include <time.h>
#include <stdio.h>
#include <string>

#include <tp5.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;


std::vector<string> TP5::names(
{
            "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
            "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
            "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
            "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
            "Fanny", "Jeanne", "Elo"
        });

unsigned long int hash(string key)
{
    // return an unique hash id from key

    int i = 0;
    int hash_value = 0;
    while(key[i] != '\0')
    {
        hash_value += (int)key[i] * pow(128, key.size() - i);
        i++;
    }

    return hash_value;

    /*
     * Avec un for
    int hash_value = 0;
    for(int i=0; i<key.length(); i++)
    {
        hash_value += (int) key[i]* pow(127, (key.length()-i));
    }
    return hash_value;
    */
}

struct MapNode : public BinaryTree
{

    string key;
    unsigned long int key_hash;

    int value;

    MapNode* left;
    MapNode* right;

    MapNode(string key, int value) : BinaryTree (value)
    {
        this->key = key;
        this->value = value;
        this->key_hash = hash(key);

        this->left = this->right = nullptr;
    }

    /**
     * @brief insertNode insert a new node according to the key hash
     * @param node
     */
    void insertNode(MapNode* node)
    {

        if(node->key_hash == this->key_hash){

        }
        else if(node->key_hash < this->key_hash){
            if(left){
                left->insertNode(node);
            }
            else{
                left = node;
            }
        }
        else{
            if(right){
                right->insertNode(node);
            }
            else{
                right = node;

            }
        }
    }

    void insertNode(string key, int value)
    {
        this->insertNode(new MapNode(key, value));
    }

    virtual ~MapNode() {}
    QString toString() const override {return QString("%1:\n%2").arg(QString::fromStdString(key)).arg(value);}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

struct Map
{
    Map() {
        this->root = nullptr;
    }

    /**
     * @brief insert create a node and insert it to the map
     * @param key
     * @param value
     */
    void insert(string key, int value)
    {
        MapNode* nouvelle_node = new MapNode(key, value);
        if(this->root == nullptr){
            this->root = nouvelle_node;
        }
        else{
            this->root->insertNode(nouvelle_node);
        }
    }

    /**
     * @brief get return the value of the node corresponding to key
     * @param key
     * @return
     */

    int get(string key)
    {

        MapNode* noeud = this->root;
        int valueHash = hash(key);

        while(noeud){

            if(valueHash == noeud->key_hash){
                return noeud->value;
            }
            else if(valueHash < noeud->key_hash){
                //if(noeud->left){
                //noeud = noeud->left;
                if(noeud->left){
                    noeud=noeud->left;
                }
                else return -1;
            }
            else{
                //if(noeud->right){
                //noeud = noeud->right;
                if(noeud->right){
                    noeud=noeud->right;
                }
                else return -1;
            }
        }
        //return 0;
        return -1;

    }

    MapNode* root;

};


int main(int argc, char *argv[])
{
    srand(time(NULL));
    Map map;

    map.insert("Yolo", 20);
    for (std::string& name : TP5::names)
    {
        if (rand() % 3 == 0)
        {
            map.insert(name, rand() % 21);
        }
    }

    printf("map[\"Margot\"]=%d\n", map.get("Margot"));
    printf("map[\"Jolan\"]=%d\n", map.get("Jolan"));
    printf("map[\"Lucas\"]=%d\n", map.get("Lucas"));
    printf("map[\"Clemence\"]=%d\n", map.get("Clemence"));
    printf("map[\"Yolo\"]=%d\n", map.get("Yolo"));
    printf("map[\"Tanguy\"]=%d\n", map.get("Tanguy"));

    printf("map[\"Marie\"]=%d\n", map.get("Marie"));
    printf("map[\"Flavien\"]=%d\n", map.get("Flavien"));
    printf("map[\"Sacha\"]=%d\n", map.get("Sacha"));
    printf("map[\"JoLeClodo\"]=%d\n", map.get("JoLeClodo"));


    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new MapWindow(*map.root);
    w->show();
    return a.exec();
}
