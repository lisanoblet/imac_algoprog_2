#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    
};

struct DynaTableau{
    int* donnees;
    
};


// PARTIE LISTE/NOEUD

void initialise(Liste* liste)
{
    /*
    Liste* nouvelle_liste = (Liste*) malloc(sizeof (Liste));
    //Noeud* nouveau_noeud = (Noeud*) malloc(sizeof (Noeud));

    if(!nouvelle_liste){
        cout << "Erreur d'allocation" << endl;
        exit(1);
    }
    if(!nouveau_noeud){
        cout << "Erreur d'allocation" << endl;
        exit(1);
    }*/
    //nouvelle_liste->nouveau_noeud->donnee=donnee;
    //nouvelle_liste->suivant=NULL;

    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if (liste==nullptr){
       return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveau_noeud = (Noeud*) malloc(sizeof (Noeud));
    
    if(!nouveau_noeud){
        std::cout << "Erreur d'allocation" << endl;
        exit(1);
    }

    nouveau_noeud->donnee = valeur;
    nouveau_noeud->suivant = nullptr;
    liste->premier = nouveau_noeud;
    
      /*
    //liste->premier->donnee=valeur;
    //liste->premier->suivant=nullptr;
    */
}

void affiche(const Liste* liste)
{
    /*while(liste != nullptr && liste->premier != nullptr && liste->premier->suivant != nullptr){ //test pour ne pas sortir des limites
     
    //while(liste->premier->suivant!=nullptr){
        std::cout << liste->premier->donnee << endl;
        //liste = liste->premier->suivant;
        *liste->premier = *liste->premier->suivant;
    }
    */
    Noeud* noeud_actuel=liste->premier;
    while(noeud_actuel!=nullptr) {
        std::cout << noeud_actuel->donnee << endl;
        noeud_actuel=noeud_actuel->suivant;
    }
    
}

int recupere(const Liste* liste, int n)
{
    while(liste != nullptr && liste->premier != nullptr && liste->premier->suivant != nullptr){ //test pour ne pas sortir des limites
     
        for(int i = 0; i < n && liste != nullptr; i++){
            *liste->premier = *liste->premier->suivant;
        }     
        return liste->premier->donnee;
    }
}

int cherche(const Liste* liste, int valeur)
{
    int index_recherche = 0;

    while(liste!=nullptr){
        if(liste->premier->donnee == valeur){
            return index_recherche;
        }
        else{
            return -1;
        }  
        *liste->premier = *liste->premier->suivant;
        index_recherche += 1;
    }
}

void stocke(Liste* liste, int n, int valeur)
{
    while(liste != nullptr && liste->premier != nullptr && liste->premier->suivant != nullptr){ //test pour ne pas sortir des limites
        for(int i = 0; i < n && liste != nullptr ; i++){
            *liste->premier = *liste->premier->suivant;
        }     
        liste->premier->donnee = valeur;
    }
}

// PARTIE DYNATABLEAU

void ajoute(DynaTableau* tableau, int valeur)
{
    int i = 0;
    while(tableau->donnees[i] != NULL){
        i++;
    }
    tableau->donnees[i] = valeur;
}


void initialise(DynaTableau* tableau, int capacite)
{
    for(int i = 0; i < capacite; i++){
        tableau->donnees[i] = NULL;
    }
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->donnees == nullptr){
        return true;
    }
        return false;
}

void affiche(const DynaTableau* tableau)
{
    int i=0;
    while(tableau->donnees[i] != NULL){
        std::cout << tableau->donnees[i] << endl;
        i++;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if(tableau->donnees[n] != NULL){
        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int i = 0;
    while(tableau->donnees[i] != NULL){
        if (tableau->donnees[i] == valeur){
            return i;
        }
        i++;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

// PARTIE PILE FILE     
//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur) //empilage
{
    Noeud* nouveau_noeud = (Noeud*) malloc(sizeof(Noeud));

    if(!nouveau_noeud){
        std::cout << "Erreur d'allocation" << endl;
        exit(1);
    }

    nouveau_noeud->donnee = valeur;
    nouveau_noeud->suivant = nullptr;

    if(liste->premier != NULL){
        
        Noeud* noeud_parcours = liste->premier;

        while(noeud_parcours->suivant != nullptr){ //on va jusqu'a la fin de la liste 
            noeud_parcours = noeud_parcours->suivant;
        }

        noeud_parcours->suivant = nouveau_noeud;
    }
    else{
        liste->premier = nouveau_noeud;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste) //depilage
{
    int donnee_retiree = 0;

    Noeud* premier_noeud = liste->premier;

    if (liste != nullptr && liste->premier != nullptr){
        donnee_retiree = premier_noeud->donnee;
        liste->premier = premier_noeud->suivant;
        free(premier_noeud);
    }
    return donnee_retiree;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur) //enfilage
{
    Noeud* nouveau_noeud = (Noeud*) malloc(sizeof(Noeud));

    if(!nouveau_noeud){
        std::cout << "Erreur d'allocation" << endl;
        exit(1);
    }

    nouveau_noeud->donnee = valeur;
    nouveau_noeud->suivant = liste->premier;
    liste->premier = nouveau_noeud;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste) //defilage
{
    int donnee_retiree = 0;

    Noeud* premier_noeud = liste->premier;

    if (liste != nullptr && liste->premier != nullptr){
        donnee_retiree = premier_noeud->donnee;
        liste->premier = premier_noeud->suivant;
        free(premier_noeud);
    }
    return donnee_retiree;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
