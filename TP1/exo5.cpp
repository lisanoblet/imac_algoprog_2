#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){

    if(n==0){
        return 0;
    }

    else{

        float module=z.length();

        if(module>2){
            return n;
        }

        else{
            float nouveau_x=z.x*z.x-z.y*z.y+point.x;
            float nouveau_y=2*z.x*z.y+point.y;
            z.x=nouveau_x;
            z.y=nouveau_y;

            return isMandelbrot(z, n-1, point);
        }
    }

    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



