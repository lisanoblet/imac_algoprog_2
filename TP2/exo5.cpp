#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size()>1){
        // initialisation
        Array& first = w->newArray(origin.size()/2);
        Array& second = w->newArray(origin.size()-first.size());

        // split

        for (int i = 0; i < origin.size()/2; i++){
            first[i] = origin[i];
        }

        for (int j = 0; j < origin.size()-first.size(); j++){
            second[j] = origin[origin.size()/2 + j];
        }

        // recursiv splitAndMerge of lowerArray and greaterArray

        splitAndMerge(first);
        splitAndMerge(second);

        // merge
        merge(first, second, origin);
    }
    else{
        return;
    }
}

void merge(Array& first, Array& second, Array& result)
{
    int i=0;
    int j=0;
    while(i != first.size() && j != second.size()){
        if(first[i] < second[j]){
            result[i+j] = first[i];
            i++;
        }
        else {
            result[i+j] = second[j];
            j++;
        }
    }
    if(i == first.size()){
        while (j != second.size()){
            result[i+j] = second[j];
            j++;
        }
    }
    else if(j == second.size()){
        while (i != first.size()){
            result[i+j] = first[i];
            i++;
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
    w->show();

    return a.exec();
}
