#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){

    for(int i = 0; i < toSort.size()-1; i++){
        int minimum = i;
        for(int j = i+1; j < toSort.size(); j++){
           if(toSort[j] < toSort[minimum]){
                minimum = j;
           }
        }
        toSort.swap(minimum, i);

    }
    // selectionSort
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}




/*void selectionSort(Array& toSort){
int indice_minimum = 0;
    for(int j = 0; j < toSort.size(); j++){
        for(int i = 1; i<toSort.size(); i++){
           if(toSort[i] < toSort[j]){
               indice_minimum = i;
           }
        }
        toSort.swap(j, indice_minimum);

}
}*/
