#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        // BinarySearchTree* nouveau;
        // Node* premier;
        this->left = nullptr;
        this->right = nullptr;
        this->value = value;

    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        //initNode(value);

        //BinarySearchTree* nouveau;
        //nouveau->initNode(value);

        if(value == this->value){

        }
        else if(value < this->value){
            if(left){
                left->insertNumber(value);
            }
            else{
                left = new BinarySearchTree(value);
                left->initNode(value);
            }
        }
        else{
            if(right){
                right->insertNumber(value);
            }
            else{
                right = new BinarySearchTree(value);
                right->initNode(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int hauteurGauche;
        int hauteurDroite;

        if(left){
            hauteurGauche = left->height();
        }
        else{
            hauteurGauche = 0;
        }

        if(right){
            hauteurDroite = right->height();
        }
        else{
            hauteurDroite = 0;
        }

        if(hauteurDroite < hauteurGauche){
            return hauteurGauche + 1;
        }
        else{
            return hauteurDroite + 1;
        }

    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int countGauche;
        int countDroite;

        if(left){
            countGauche = left->nodesCount();
        }
        else{
            countGauche = 0;
        }

        if(right){
            countDroite = right->nodesCount();
        }
        else{
            countDroite = 0;
        }

        return countDroite + countGauche + 1;
    }

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)

        if (!left && !right){
            return true;
        }

        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        if(this->isLeaf()){
            leaves[leavesCount] = this;
            leavesCount++;
        }

        if(left){
            left->allLeaves(leaves, leavesCount);
        }

        if(right){
            right->allLeaves(leaves, leavesCount);
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel

        if(left){
            left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount ++;

        if(right){
            right->inorderTravel(nodes, nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel

        nodes[nodesCount] = this;
        nodesCount ++;

        if(left){
            left->preorderTravel(nodes, nodesCount);
        }

        if(right){
            right->preorderTravel(nodes, nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if(left){
            left->postorderTravel(nodes, nodesCount);
        }

        if(right){
            right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount ++;
	}

	Node* find(int value) {
        // find the node containing value

        if(value == this->value){
            return this;
        }
        else if(value < this->value){
            if(left){
                return left->find(value);
            }
        }
        else{
            if(right){
                return right->find(value);
            }
        }

        return nullptr;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
