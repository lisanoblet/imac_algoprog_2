#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
    /**
      * Make a graph from a matrix
      * first create all nodes, add it to the graph then connect them
      * this->appendNewNode
      * this->nodes[i]->appendNewEdge
      */

    for(int i = 0; i < nodeCount; i++){
        this->appendNewNode(new GraphNode(i));
    }

    for(int i = 0; i < nodeCount; i++){
        for(int j = 0; j < nodeCount; j++){
            if(adjacencies[i][j] >= 1){
                this->nodes[i]->appendNewEdge(this->nodes[j], adjacencies[i][j]);
            }
        }
    }
}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
    /**
      * Fill nodes array by travelling graph starting from first and using recursivity
      */


    //visited[first] = true;
    //visited[this->nodes[i]] = false;
    /*
    for(int i = 0; i < nodesSize; i++){
        //if(!this->nodes[i]){
        if(!this->nodes[i]){
            nodesSize++;
            deepTravel(this->nodes[i+1], nodes, nodesSize, visited);
            visited[this->nodes[i]] = true;
        }
    }*/

    visited[first->value] = true;
    nodes[nodesSize] = first;
    nodesSize++;

    for(Edge* e = first->edges; e != NULL; e = e->next){
        if(!visited[e->destination->value]){
            this->deepTravel(e->destination, nodes, nodesSize, visited);
        }
    }
    return;
}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
    /**
     * Fill nodes array by travelling graph starting from first and using queue
     * nodeQueue.push(a_node)
     * nodeQueue.front() -> first node of the queue
     * nodeQueue.pop() -> remove first node of the queue
     * nodeQueue.size() -> size of the queue
     */
    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);

    visited[first->value] = true;
    //nodes[nodesSize] = first;
    //nodesSize++;


    /* La fonction marche mais parfois le programme plante à cause de la ligne 85 */

    while(!nodeQueue.empty()){

        first = nodeQueue.front();
        nodeQueue.pop();

        visited[first->value] = true;
        nodes[nodesSize] = first;
        nodesSize++;

        for(Edge* e = first->edges; e != NULL; e = e->next){
            if(!visited[e->destination->value]){
                nodeQueue.push(e->destination);
            }
        }
    }
}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
    /**
      Detect if there is cycle when starting from first
      (the first may not be in the cycle)
      Think about what's happen when you get an already visited node
    **/

    visited[first->value] = true;

    for(Edge* e = first->edges; e != NULL; e = e->next){
        if(!visited[e->destination->value]){
            //this->detectCycle(e->destination, visited);
            return true;
        }
    }
    return false;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 100;
    w = new GraphWindow();
    w->show();

    return a.exec();
}
